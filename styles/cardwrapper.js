import styled from "styled-components";

const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 360px;
  min-height: auto;
  border-radius: 8px;
  display: block;
  overflow: hidden;
  position: relative;
  transition: box-shadow 0.2s;
  margin: 0 auto;
  background: #fff;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.12);

  @media (max-width: 768px) {
    background: #fff;
    }
`;

export default CardWrapper;
