import styled from "styled-components";

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  width: 100%;
  margin-bottom: 4rem;
  @media (min-width: 880px) {
    flex-direction: row;
  }
`;

export default Content;
