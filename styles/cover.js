import styled from "styled-components";

const Cover = styled.div`
  background-color: #f8f9fa;
  height: 250px;
  margin: 0;
  padding: 0;
  overflow: hidden;
  text-align: center;
  & img {
    height: 130%;
    object-fit: cover;
    width: 100%;
    display: inline-block;
    max-width: 100%;
    vertical-align: middle;
  }
`;

export default Cover;
