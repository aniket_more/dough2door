import styled from "styled-components";

const Description = styled.div`
  border-radius: 0 0 5px 5px;
  box-shadow: 0 0 0.5rem rgba(0, 0, 0, 0.07);
  background: #fff;
  & h1 {
    text-align: center;
    background: #ffcb009c;
    color: #2d2d2d;
    margin: 0;
    padding: 8px 0;
    font-weight: 700;
    font-size: 22px;
    line-height: 1.33333;
  }
  .info {
    display: flex;
    flex-direction: column;
    border-radius: 0 0 4px 4px;
    height: 100%;
    padding: 0.5rem 0.1rem;
  }
  .quantity {
    max-width: 100%;
    display: flex;
    padding: 1.3rem 5px;
  }
  .q1 {
    width: 50%;
    padding-left: 10px;
    color: #4e4e4e;
    font-weight: 700;
    font-size: 1.1rem;
  }
  .q2 {
    width: 50%;
    padding-left: 5px;
    color: #4caf50;
    font-weight: 700;
    font-size: 1.1rem;
  }
  @media (max-width: 768px) {
    .q1,
    .q2 {
      font-size: 0.9rem;
    }
  }
  @media (max-width: 360px) {
    .q1,
    .q2 {
      font-size: 0.8rem;
    }
  }
  p {
    margin: 16px 0;
  }
`;

export default Description;
