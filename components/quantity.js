import React from "react";
import styled from "styled-components";


const DecrementButton = styled.button`
  cursor: pointer;
  margin: 0 20px;
  margin-left: 0;
  padding: 0 14px;
  border-radius: 3px;
  border: 1px solid #f44336;
  font-weight: bold;
  color: white;
  background-color: indianred;

  @media (max-width: 390px) {
    padding: 0 10px;
  }
`;

const IncrementButton = styled.button`
  cursor: pointer;
  margin: 0 20px;
  padding: 0 14px;
  border-radius: 3px;
  border: 1px solid #4caf50;
  font-weight: bold;
  color: white;
  background-color: #4caf50;

  @media (max-width: 390px) {
    padding: 0 10px;
  }
`;

const Count = styled.span`
  font-size: 1.5rem;
`;

const Quantity = ({ quantity, setQuantity }) => {
  function increment(e) {
    if (quantity < 3) {
      setQuantity(quantity + 1);
    }
  }

  function decrement(e) {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  }

  return (
    <>
      <DecrementButton onClick={decrement}> - </DecrementButton>
      <Count>{quantity}</Count>
      <IncrementButton onClick={increment}> + </IncrementButton>
    </>
  );
};
export default Quantity;
