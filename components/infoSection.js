export default function InfoSection() {
  return (
    <>
      <section>
        <div className="wrapper">
          <ul className="notable">
            <li>Minimum Monthly Subscription should be &#8377;2000/-</li>
            <li>
              Any ammendments in Subscription orders must be made a week in
              advance.
            </li>
            <li>Ammendment may be made only once a month.</li>
            <li>
              A-La-Carte orders from our extensive menu, outside of the
              subscription will be charged separately.
            </li>
          </ul>
        </div>
        <div className="logoContainer">
          <img src="/images/dough2door.svg" className="logo" />
        </div>
        <style jsx>{`
          section {
            max-width: 100%;
            padding: 5rem 0 3rem;
            background: url("/images/so-white.png");
          }

          .wrapper {
            border-radius: 0.4rem;
            border-color: #54c7ec;
            padding: 1rem;
            border-left: 6px solid #f78d1e;
            background: #ffffff;
            max-width: 50rem;
            margin: 0 auto;
            box-shadow: 0 5px 10px rgba(0,0,0,.12);
          }
          .notable {
            display: block;
            list-style-type: disc;
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            padding-inline-start: 40px;
            padding-left: 1rem;
          }
          .notable > li {
            margin-bottom: 1rem;
          }
          .logo {
            width: 165px;
            height: 85px;
            cursor: pointer;
            margin: 0 auto;
          }
          .logoContainer {
            padding-top: 3rem;
          }
        `}</style>
      </section>
    </>
  );
}
