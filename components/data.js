export const Data = [
  {
    ProductCode: "B001",
    ProductName: "Goan POI Regular",
    ProductCategory: "Bread",
    Price: 560,
    QuantityDescription: "3 Nos.",
    ImageDescription: "goan_poi_regular.jpeg",
  },
  {
    ProductCode: "B002",
    ProductName: "Milk Bread",
    ProductCategory: "chocolate",
    Price: 65,
    QuantityDescription: "400gm",
    ImageDescription: "milk_bread.jpeg",
  },
];

export const options = [
  { value: "#", label: "All" },
  { value: "Bread", label: "Bread" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];
