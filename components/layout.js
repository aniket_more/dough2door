import Head from "next/head";
import styles from "./layout.module.css";
import Link from "next/link";
import cn from "classnames";

const name = "dough2door";
export const siteTitle = "dough2door Monthly Subscription";

export default function Layout({ children, home }) {
  return (
    <div>
      <Head>
        <meta name="description" content="dough2door Monthly Subscription" />
        <script src="https://js.instamojo.com/v1/checkout.js"></script>
      </Head>
      <div className={styles.headerContainer}>
        <header className={cn(styles.header)}>
          {home ? (
            <>
              <Link href="/">
                <img
                  src="/images/dough2door.svg"
                  className={`${styles.headerHomeImage}`}
                  alt={name}
                />
              </Link>
              <h1 className={styles.subHeading}>ALWAYS FRESH</h1>
            </>
          ) : (
            <>
              <Link href="/">
                <img
                  src="/images/dough2door.svg"
                  className={`${styles.headerHomeImage}`}
                  alt={name}
                />
              </Link>
              <h2 className={styles.subHeading}>
                <Link href="/">
                  <a>{name}</a>
                </Link>
              </h2>
            </>
          )}
        </header>
      </div>

      {children}

      {!home && (
        <div className={styles.backToHome}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </div>
      )}
    </div>
  );
}
