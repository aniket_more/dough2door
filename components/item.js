import React, { useState, useEffect } from "react";
import styled from "styled-components";
import DatePicker from "../components/DatePicker";
import Quantity from "../components/quantity";
import AddRemoveButtons from "../components/addRemoveButton";
import CardWrapper from "../styles/cardwrapper";
import Description from "../styles/description";
import Content from "../styles/content";
import Cover from "../styles/cover";

//Lodash imports
import { get, findIndex, pullAllBy, last, differenceBy, isEqual } from "lodash";

const Price = styled.div`
  p {
    text-align: right;
    margin-right: 1.5rem;
    margin-top: 0.5rem;
  }

  @media (max-width: 768px) {
    p {
      text-align: center;
      margin-right: 0;
      font-size: 1rem;
    }
  }
`;

const Item = ({
  price = 45,
  imageURL = "/images/goan_poi_regular.jpeg",
  imageDesc = "Goan POI Regular",
  id = "goan",
  qtyPerOrder = 3,
  order = [],
  setOrder = (f) => f,
}) => {
  const [currentQty, setCurrentQty] = useState([]);
  const [currentOrder, setCurrentOrder] = useState(
    get(order[id], "order") || []
  );
  const [current, setCurrent] = useState();

  useEffect(() => {
    const temp = {...order};
    if (id in temp) temp[id].total = totalPrice(currentOrder);
    setOrder(temp);
  }, [currentOrder]);

  //On date selection, set current date as current and create an currentOrder
  //Single function handles date add and remove
  const onDateSelect = (value) => {
    let tempOrder = [...currentOrder];
    if (value.length > currentOrder.length) tempOrder = [last(value)];
    else {
      tempOrder = differenceBy(tempOrder, value, "date");
    }
    const { day, month, year } = tempOrder[0];

    const index = findIndex(currentOrder, { date: `${day}-${month}-${year}` });
    if (index === -1) {
      setCurrentOrder((oldOrder) => [
        ...oldOrder,
        {
          quantity: 0,
          date: `${day}-${month}-${year}`,
          day: day,
          month: month,
          year: year,
        },
      ]);
      setCurrentQty(0);
      setCurrent(`${day}-${month}-${year}`);
    } else {
      setCurrentQty(currentOrder[index].quantity);
      setCurrent(`${day}-${month}-${year}`);
    }
  };

  //On item add set date and update quantity
  const onAdd = () => {
    const tempOrder = [...currentOrder];
    if (currentQty === 0) pullAllBy(tempOrder, [{ date: current }], "date");
    else {
      tempOrder[
        findIndex(currentOrder, { date: current })
      ].quantity = currentQty;
    }
    setCurrentOrder(tempOrder);
    setCurrent();
    setOrder((order) => ({
      ...order,
      [id]: { order: tempOrder },
    }));
  };

  //On item remove, remove currentOrder
  const onRemove = () => {
    const tempOrder = [...currentOrder];
    pullAllBy(tempOrder, [{ date: current }], "date");
    setCurrentOrder(tempOrder);
    setCurrent();
    setOrder((order) => ({
      ...order,
      [id]: { order: tempOrder },
    }));
  };

  //Remove currentOrders which dont have any quantity
  const selectedDate = (currentOrder) => {
    const temp = [...currentOrder];
    pullAllBy(temp, [{ quantity: 0 }], "quantity");
    return temp;
  };

  //Calculate total price
  const totalPrice = (currentOrder) => {
    let tPrice = 0;
    currentOrder.forEach((element) => {
      tPrice = tPrice + element.quantity * price;
    });
    return tPrice;
  };

  //On clear all
  const onClearAll = () => {
    setCurrentOrder([]);
    setCurrent();
    setCurrentQty();
  };

  const onCancel = () => {
    if (
      currentQty === 0 &&
      currentOrder[findIndex(currentOrder, { date: current })].quantity === 0
    )
      onRemove();
    else {
      setCurrent();
    }
  };
  return (
    <>
      <Content>
        <CardWrapper>
          <Cover>
            <img src={imageURL} alt={imageDesc} />
          </Cover>
          <Description>
            <h1>{imageDesc}</h1>
            <div className="info">
              <div className="quantity">
                <div className="q1">Price(&#8377;)</div>
                <div className="q2">{price}</div>
              </div>
              {currentOrder.length > 0 && (
                <div className="quantity">
                  <div className="q1">Total(&#8377;)</div>
                  <div className="q2">{totalPrice(currentOrder)}</div>{" "}
                </div>
              )}
              {/* <button onClick={onClearAll}>Remove All</button> */}
            </div>
          </Description>
        </CardWrapper>
        <CardWrapper>
          <div style={current ? { pointerEvents: "none", opacity: "0.4" } : {}}>
            {/* {!current && ( */}
            <DatePicker
              selectedDays={selectedDate(currentOrder)}
              setSelectedDays={onDateSelect}
            />
            {/* )} */}
          </div>

          <Description>
            {current && (
              <React.Fragment>
                <div className="quantity">
                  <div className="q1">Date</div>
                  <div className="q2">{current}</div>
                </div>
                <div className="quantity">
                  <div className="q1">QTY ({qtyPerOrder})</div>
                  <Quantity
                    quantity={currentQty}
                    setQuantity={setCurrentQty}
                    current={current}
                  />
                </div>
              </React.Fragment>
            )}
          </Description>
          <div style={{ textAlign: "center", padding: "5px 0" }}>
            <AddRemoveButtons
              onAdd={onAdd}
              onRemove={onRemove}
              onClearAll={onClearAll}
              onCancel={onCancel}
              current={current}
              showClear={currentOrder.length !== 0}
            />
          </div>
        </CardWrapper>
      </Content>
    </>
  );
};
export default Item;

//
