import React from "react";
import { Calendar, utils } from "react-modern-calendar-datepicker";

const DatePicker = ({ selectedDays, setSelectedDays }) => {
  const maximumDate = {
    year: new Date().getFullYear(),
    month: new Date().getMonth()+2,
    day: 31,
  };
  const minimumDate = {
    year: new Date().getFullYear(),
    month: new Date().getMonth()+1,
    day: new Date().getDate(),
  };

  //console.log(selectedDays);

  return (
    <Calendar
      value={selectedDays}
      onChange={setSelectedDays}
      minimumDate={minimumDate}
      maximumDate={maximumDate}
      calendarClassName="responsive-calendar" // added this for reponsive calendar
      shouldHighlightWeekends
    />
  );
};

export default DatePicker;
