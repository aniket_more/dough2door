import { orderBy } from "lodash";
import React from "react";
import styled from "styled-components";

const ClearButton = styled.button`
  cursor: pointer;
  margin: 0 10px;
  padding: 0 14px;
  border-radius: 3px;
  border: 1px solid #f44336;
  font-weight: bold;
  color: white;
  background-color: indianred;

  @media (max-width: 390px) {
    padding: 0 10px;
  }
`;

const DecrementButton = styled.button`
  cursor: pointer;
  margin: 0 10px;
  padding: 0 14px;
  border-radius: 3px;
  border: 1px solid #f44336;
  font-weight: bold;
  color: white;
  background-color: orange;

  @media (max-width: 390px) {
    padding: 0 10px;
  }
`;

const IncrementButton = styled.button`
  cursor: pointer;
  margin: 0 10px;
  padding: 0 14px;
  border-radius: 3px;
  border: 1px solid #4caf50;
  font-weight: bold;
  color: white;
  background-color: #4caf50;

  @media (max-width: 390px) {
    padding: 0 10px;
  }
`;

const Count = styled.span`
  font-size: 1.5rem;
`;

const Quantity = ({
  onAdd = (f) => f,
  onRemove = (f) => f,
  current,
  onClearAll = (f) => f,
  onCancel = (f) => f,
  showClear,
}) => {
  return (
    <>
      {showClear && !current && (
        <ClearButton onClick={onClearAll}> Clear </ClearButton>
      )}
      {current && (
        <React.Fragment>
          <ClearButton onClick={onCancel}>Close</ClearButton>
          <DecrementButton onClick={onRemove}>Remove</DecrementButton>
          <IncrementButton onClick={onAdd}>Add</IncrementButton>
        </React.Fragment>
      )}
    </>
  );
};
export default Quantity;
