import React from "react";
import { useForm } from "react-hook-form";

const dummyGoanOrder = `Goan Poi Bread: 12-12-2020:Qty 1, 13-12-2020:Qty 2,`;

export default function Form({ order = [], total }) {
  const { register, errors, watch } = useForm({
    mode: "onChange",
  });
  const watchAllFields = watch();
  const sendName = watchAllFields.name;
  const sendEmail = watchAllFields.email;

  const mapOrderToString = (order) => {
    let temp = "";
    for (const item in order) {
      temp = temp.concat(`Product Code:${item} \r\n`);
      order[item].order.forEach(
        (element) =>
          (temp = temp.concat(
            ` Date: ${element.date} --> Qty: ${element.quantity}\r\n`
          ))
      );
      temp = temp.concat(`\r\n`);
    }
    return temp;
  };

  let action = `/success?amount=${total}&name=${sendName}&email=${sendEmail}`;

  return (
    <>
      <form
        data-netlify="true"
        action={action}
        method="POST"
        name="contact"
        data-netlify-honeypot="bot-field"
      >
        <input type="hidden" name="form-name" value="contact" />

        {/*MAIN FORM */}

        <div
          style={{
            textAlign: "center",
            fontSize: "1.5rem",
            marginBottom: "3rem",
            marginTop: "3rem",
            color: "#525252",
          }}
        >
          Please fill in the details and submit the form.
        </div>
        <fieldset>
          {/*******NAME********/}
          <label htmlFor="name">Full Name</label>
          <input
            type="text"
            name="name"
            ref={register({ required: "Name is required", maxLength: 180 })}
          />
          {errors.Name && <p>{errors.Name.message}</p>}
        </fieldset>

        <fieldset>
          {/*******EMAIL********/}
          <label htmlFor="email">Email</label>
          <input
            type="text"
            name="email"
            ref={register({
              required: "Email is required",
              pattern: {
                value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                message: "Invalid email address",
              },
            })}
          />
          {errors.email && <p>{errors.email.message}</p>}
        </fieldset>

        <fieldset>
          {/*******PHONE********/}
          <label>Phone number</label>
          <input
            type="tel"
            name="phoneNumber"
            ref={register({
              required: {
                value: true,
                message: "Phone number is required",
              },
              minLength: {
                value: 10,
                message: "Phone number should be 10 digits long.",
              },
            })}
          />
          {errors.phoneNumber && <p>{errors.phoneNumber.message}</p>}
        </fieldset>

        <fieldset>
          {/*******ADDRESS********/}
          <label htmlFor="address">Address</label>
          <textarea
            rows="5"
            name="address"
            ref={register({ required: "Address is required" })}
          />
          {errors.address && <p>{errors.address.message}</p>}
        </fieldset>

        {/* AUXILLARY INFO */}

        <input
          id="order"
          name="order"
          value={mapOrderToString(order)}
          style={{ opacity: "0", height: "0", display: "none" }}
        ></input>

        <input
          id="amount"
          name="amount"
          value={total}
          style={{ opacity: "0", height: "0", display: "none" }}
        ></input>

        <button type="submit">Submit</button>
      </form>
      <style jsx>{`
        form {
          max-width: 50rem;
          max-width: 675px;
          padding: 1.5rem 3rem 4rem;
          margin: 0 auto;
          font-weight: 700;
          background: #fdfdfd;
          box-shadow: 10px 2px 10px rgba(82 82 82 / 30%);
          border-left: 2px solid orange;
        }

        form p {
          color: #bf1650;
          font-size: 0.8rem;
        }

        form p::before {
          display: inline;
          content: "⚠ ";
        }

        fieldset {
          border: medium none !important;
          margin: 0 0 10px;
          min-width: 100%;
          padding: 0;
          width: 100%;
        }

        input {
          display: block;
          box-sizing: border-box;
          margin-bottom: 10px;
          height: 56px;
          display: block;
          width: 100%;
          background: #fff;
          line-height: 1.2;
          border: 1px solid #ccc;
          padding: 0 16px 0 16px;
          border-radius: 3px;
        }

        label {
          line-height: 2;
          text-align: left;
          display: block;
          margin-bottom: 10px;
          margin-top: 20px;
          color: #525252;
          font-size: 18px;
          font-weight: 700;
        }

        textarea {
          display: block;
          box-sizing: border-box;
          width: 100%;
          margin-bottom: 10px;
          font-size: 16px;
          border: 1px solid #ccc;
          padding: 16px;
          border-radius: 3px;
          background: #fff;
        }

        ::placeholder {
          color: #9e9e9e;
          font-weight: 400;
          font-size: 16px;
        }

        input[type="submit"] {
          background: #ec5990;
          color: black;
          text-transform: uppercase;
          border: none;
          margin-top: 40px;
          padding: 20px;
          font-size: 16px;
          font-weight: 700;
          letter-spacing: 10px;
        }

        input[type="submit"]:hover {
          background: #bf1650;
        }

        input[type="button"]:active,
        input[type="submit"]:active {
          transition: 0.3s all;
          transform: translateY(3px);
          border: 1px solid transparent;
          opacity: 0.8;
        }

        input:disabled {
          opacity: 0.4;
        }

        input[type="button"]:hover {
          transition: 0.3s all;
        }

        input[type="button"],
        input[type="submit"] {
          -webkit-appearance: none;
        }

        button[type="submit"] {
          display: block;
          background: #0069ed;
          color: #ffffff;
          padding: 18px 10px;
          margin-top: 40px;
          width: 100%;
          border-radius: 6px;
          font-size: 20px;
          font-family: "Helvetica", Arial, sans-serif;
          cursor: pointer;
          transition: all 0.5s;
          position: relative;
          border: none;
        }

        button[type="submit"]::before {
          content: "";
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          z-index: 1;
          background-color: rgba(255, 255, 255, 0.1);
          transition: all 0.3s;
        }
        button[type="submit"]:hover::before {
          opacity: 0;
          transform: scale(0.5, 0.5);
        }
        button[type="submit"]::after {
          content: "";
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          z-index: 1;
          opacity: 0;
          transition: all 0.3s;
          transform: scale(1.2, 1.2);
        }
        button[type="submit"]:hover::after {
          opacity: 1;
          transform: scale(1, 1);
        }
      `}</style>
    </>
  );
}
