const express = require("express");
const next = require("next");
const bodyParser = require("body-parser");
const Insta = require("instamojo-nodejs");
require("dotenv").config();

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();


const api_key = process.env.API_KEY;
const auth_key = process.env.AUTH_KEY;
Insta.setKeys(api_key, auth_key);

const data = new Insta.PaymentData();

app.prepare().then(() => {
  const server = express();
  server.use(bodyParser.urlencoded({ extended: true }));
  server.use(bodyParser.json());

  server.get("/", (req, res) => {
      return app.render(req, res, "/", req.query);
    });

  server.post("/api/pay", (req, res) => {

     // Insta.isSandboxMode(true);

      data.amount = req.body.amount; // REQUIRED
      data.purpose = req.body.purpose; // REQUIRED
      data.email = req.body.email;
      //data.phone = req.body.phone;
      data.buyer_name = req.body.buyer_name;
      data.currency = "INR";
      data.allow_repeated_payments = "false";
      data.redirect_url = req.body.redirect_url;

      Insta.createPayment(data, function (error, response) {
        if (error) {
          // some error
          //console.log(error);
        } else {
          //console.log(response);
          // Payment redirection link at response.payment_request.longurl
          const responseData = JSON.parse(response);
          const redirectUrl = responseData.payment_request.longurl;
          res.status(200).json(redirectUrl);
          // res.send("Please check your email to make payment")
          //console.log(redirectUrl);
        }
      });
  });

    server.get("/api/paymentsuccess/", (req, res) => {
      res.send("success");
      return app.render(req, res, "/", req.query);
    });

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});

