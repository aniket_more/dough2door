import Head from "next/head";
import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import Layout, { siteTitle } from "../components/layout";
import Form from "../components/form";
import Item from "../components/item";
import InfoSection from "../components/infoSection";
import { store } from "react-notifications-component";
import { Data, options } from "../components/data";

import Select from "react-select";
const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop);

const TotalPrice = styled.p`
  margin: 0 auto;
  display: inline-block;
  position: relative;
  color: #fff;
  line-height: 1;
  position: relative;
  z-index: 10;
  padding: 0.5rem 1rem;
  font-size: 1.5rem;

  &::after {
    background: #1871da;
    transform: rotate(0deg);
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: -4;
  }

  &::before {
    background: #ffb000;
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    transform: rotate(3deg);
    z-index: -4;
  }
`;

const ProductSection = styled.section`
  max-width: 50rem;
  padding: 0 1rem;
  margin: 0 auto;
  margin-bottom: 5rem;
`;

const ProductSectionHeader = styled.header`
  & > h1 {
    font-size: 1.8rem;
    line-height: 1.2;
    font-weight: 800;
    letter-spacing: -0.05rem;
    margin: 1rem 0;
    color: #272727;
    letter-spacing: 0.2px;
    text-align: center;
    margin-bottom: 4rem;
  }
  @media (max-width: 1200px) {
    & > h1 {
      font-size: 1.2rem;
      margin-bottom: 3rem;
    }
  }

  @media (max-width: 560px) {
    & > h1 {
      font-size: 0.9rem;
      margin-bottom: 3rem;
    }
  }
  span.month {
    color: #e73825;
  }

  span.title {
    color: #f78d1e;
  }

  p {
    text-align: center;
    font-size: 1.2rem;
    margin-bottom: 4rem;
  }
`;
let key;

export default function Home() {
  useEffect(() => {
    store.addNotification({
      message: "Your total order amount should be more than ₹1000",
      type: "info",
      insert: "bottom",
      container: "top-full",
      animationIn: ["animate__animated", "animate__fadeIn"],
      animationOut: ["animate__animated", "animate__fadeOut"],
      dismiss: {
        duration: 3000,
        onScreen: false,
        pauseOnHover: true,
      },
    });
  }, []);
  const myRef = useRef(null);
  const executeScroll = () => {
    scrollToRef(myRef);
  };
  const [grandTotal, setGrandTotal] = useState();
  const [order, setOrder] = useState({});
  const [selectedCat, setSelectedCat] = useState({ value: "#", label: "All" });

  useEffect(() => {
    let temp = 0;
    for (const item in order) {
      if (order[item]) temp = temp + order[item].total;
    }
    setGrandTotal(temp);
  }, [order]);

  useEffect(() => {
    if (grandTotal > 1000 && key === undefined)
      key = store.addNotification({
        message: "Order Value more than ₹1000, proceed to payment",
        type: "success",
        insert: "top",
        container: "top-full",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
      });
    if (grandTotal < 1000) {
      store.removeNotification(key);
      key = undefined;
    }
  }, [grandTotal]);

  const minOrderValue = 1000;

  const onCatChange = (value) => {
    setSelectedCat(value.target.value);
  };

  const customControlStyles = (base) => ({
    height: 50,
  });
  const selectStyles = { menu: (styles) => ({ ...styles, zIndex: 999 }) };

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
        <meta name='viewport' 
             content='width=device-width, initial-scale=1.0, maximum-scale=1.0, 
                  user-scalable=0' />
                  
      </Head>
      <main>
        <ProductSection>
          <ProductSectionHeader>
            <h1>
              <span className="title">MONTHLY SUBSCRIPTION</span> -{" "}
              <span className="month">
                {new Date().toLocaleString("default", { month: "short" })}{" "}
                {new Date().getFullYear()}
              </span>
            </h1>
            <div
              style={{
                width: "20rem",
                margin: "0.5rem auto",
                alignContent: "center",
              }}
            >
              <Select
                value={selectedCat}
                onChange={setSelectedCat}
                options={options}
                styles={selectStyles}
              />
            </div>

            <p>
              Please select the dates using the calendar in front of the breads
              of your choice.
            </p>
          </ProductSectionHeader>
          {Data.filter(
            (item) =>
              item.ProductCategory === selectedCat.value ||
              selectedCat.value === "#"
          ).map((item) => (
            <Item
              id={item.ProductCode}
              key={item.ProductCode}
              price={item.Price}
              imageURL={`/images/${item.ImageDescription}`}
              imageDesc={item.ProductName}
              qtyPerOrder={item.QuantityDescription}
              order={order}
              setOrder={setOrder}
            />
          ))}
          <div className="total">
            {" "}
            <TotalPrice>Your Total is &#8377;{grandTotal}</TotalPrice>
          </div>
          {grandTotal < minOrderValue && (
            <div style={{ textAlign: "center" }}>
              <span style={{ color: "orange" }}>
                Note: Your total amount should be more than &#8377;
                {minOrderValue} to place order.
              </span>
            </div>
          )}
        </ProductSection>
        <div ref={myRef}>
          <section
            className="formSection"
            style={
              grandTotal <= minOrderValue
                ? { pointerEvents: "none", opacity: "0.4" }
                : {}
            }
          >
            <Form order={order} total={grandTotal} id="checkout" />
          </section>
        </div>
        <div
          style={
            grandTotal < minOrderValue
              ? { pointerEvents: "none", opacity: "0.4" }
              : {}
          }
        >
          <a onClick={executeScroll} class="float">
            <img src='/cart.svg' />
          </a>
        </div>

        <style jsx>{`
          .formSection {
            max-width: 100%;
            position: relative;
            border-bottom: 1px solid orange;
            background: #fdfdfd;
            background: url("/images/checkers.jpg");
            background-repeat: repeat;
          }
          .formSection::before {
            background-color: #f78d1e;
            border: 1px solid #f78d1e;
            border-radius: 3px 3px 0px 0px;
            content: "";
            height: 3px;
            left: -1px;
            position: absolute;
            right: -1px;
            top: -1px;
          }
          .total {
            display: flex;
            padding: 3rem 0.5rem;
          }
          main {
            padding-top: 5rem;
            background: #fffaf7;
          }
        `}</style>
        <InfoSection />
      </main>
    </Layout>
  );
}
