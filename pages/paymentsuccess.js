import Head from "next/head";
import Layout from "../components/layout";
import React from "react";
import Link from "next/link";


export default function PaymentSuccess() {

  return (
    <Layout home>
      <Head>
        <title>Payment Success</title>
      </Head>
      <main>
        <section>
          <img src="/images/checked.svg" className="check" />
          <p className="success">Your payment is successful!</p>

          <p className="para">
            Please check your email for invoice.
          </p>

          <div className="backToHome">
            <Link href="/">
              <a>← Back to home</a>
            </Link>
          </div>
        </section>
        <style jsx>{`
          section {
            max-width: 50rem;
            padding: 0 1rem;
            margin: 0 auto;
            font-size: 1.2rem;
            line-height: 1.5;
            background: #fff;
            padding-bottom: 3rem;
          }
          .success {
            font-size: 1.8rem;
            line-height: 1.2;
            font-weight: 800;
            letter-spacing: -0.05rem;
            margin: 1rem 0;
            color: #272727;
            letter-spacing: 0.2px;
            text-align: center;
            margin-bottom: 4rem;
            color: green;
          }
          .backToHome {
            margin: 3rem auto 0;
            text-align: center;
          }
          .check {
            max-width: 100px;
            margin: 0 auto;
            amrgin-top: 4rem;
          }
          main {
            background: #fff;
          }
          .para {
            text-align: center;
            font-size: 1.2rem;
            margin-bottom: 4rem;
          }
        `}</style>
      </main>
    </Layout>
  );
}
