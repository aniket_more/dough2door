import Head from "next/head";
import Layout from "../components/layout";
import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";

export default function SuccessPage() {

  const router = useRouter();
  const amount = router.query.amount;
  const buyer_name = router.query.name;
  const email = router.query.email;
  //const phone = router.query.email;
  console.log(amount);
  console.log(email);
  console.log(buyer_name);

  const handleClick = () => {
    const data = {
      purpose: "dough2door - Fresh Bread Payment",
      amount: amount,
      email: email,
      //phone: phone,
      buyer_name: buyer_name,
      redirect_url: `https://subscription.dough2door.in/paymentsuccess`,
    };

    axios.post("/api/pay/", data)
      .then((res) => {
        console.log(res, res.data);
        window.location.href = res.data;
      })
      .catch((error) => console.log(error));
  }



  return (
    <Layout home>
      <Head>
        <title>Thank you page</title>
      </Head>
      <main>
        <section>
          <img src="/images/Baker.gif" className="bread" />
          <p className="success">Successfully placed your order!</p>

          <p className="para">
            Your order is pending with us. We will get back to you soon, with
            your order details and payment method.
          </p>
          <div className="buttonWrapper">
            <button id="payButton" onClick={handleClick}>
              Generate Payment Link
            </button>
          </div>
          <div className="backToHome">
            <Link href="/">
              <a>← Back to home</a>
            </Link>
          </div>
        </section>
        <style jsx>{`
          #payButton {
            display: inline-block;
            border: none;
            padding: 1rem 1.5rem;
            margin: 0;
            text-decoration: none;
            background: #0069ed;
            color: #ffffff;
            font-family: sans-serif;
            font-size: 1rem;
            line-height: 1;
            cursor: pointer;
            text-align: center;
            transition: background 250ms ease-in-out, transform 150ms ease;
            -webkit-appearance: none;
            -moz-appearance: none;
          }

          #payButton:hover,
          #payButton:focus {
            background: #0053ba;
          }

          #payButton:focus {
            outline: 1px solid #fff;
            outline-offset: -4px;
          }

          #payButton:active {
            transform: scale(0.99);
          }
          section {
            max-width: 50rem;
            padding: 0 1rem;
            margin: 0 auto;
            font-size: 1.2rem;
            line-height: 1.5;
            background: #fff;
            padding-bottom: 3rem;
          }
          .success {
            font-size: 1.8rem;
            line-height: 1.2;
            font-weight: 800;
            letter-spacing: -0.05rem;
            margin: 1rem 0;
            color: #272727;
            letter-spacing: 0.2px;
            text-align: center;
            margin-bottom: 4rem;
            color: green;
          }
          .backToHome {
            margin: 3rem auto 0;
            text-align: center;
          }
          .bread {
            max-width: 300px;
            margin: 0 auto;
          }
          main {
            background: #fff;
          }
          .buttonWrapper {
            display: flex;
            justify-content: center;
          }
          .para {
            margin-bottom: 4rem;
            text-align: center;
            font-size: 1.2rem;
          }
        `}</style>
      </main>
    </Layout>
  );
}
