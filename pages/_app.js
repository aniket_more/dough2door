import "../styles/reset.css";
import "../styles/DatePicker.css";
import "../styles/FloatButton.css";

import ReactNotification from "react-notifications-component";

import "react-notifications-component/dist/theme.css";

export default function App({ Component, pageProps }) {
  return (
    <div className="app-container">
      <ReactNotification />
      <Component {...pageProps} />
    </div>
  );
}
