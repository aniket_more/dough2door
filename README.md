# dough2door

[![Netlify Status](https://api.netlify.com/api/v1/badges/eb7d434b-8403-41f4-b216-bd8c44e8cd42/deploy-status)](https://app.netlify.com/sites/d2dsubscribe/deploys)

This website is made using [Next.js.](https://nextjs.org/) and hosted on [Netlify](https://www.netlify.com/).

## Development:

Clone the repo and run `npm install`.

Use:

`npm run dev` to run development server.

--

## Updating Products

1. Update changes in the excel and save it as csv.
2. Open csv using text edit and copy content.
3. Convert csv to json using [CSV to Json Parser](https://csvjson.com/csv2json) 
4. Copy output and paste in /components/data.js --> data

